﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Vinoramix.Web.Models;

namespace Vinoramix.Web.Services
{
    public class WineService
    {
        private readonly HttpClient _client;
        private readonly IHttpContextAccessor _httpContextAccessor;

        // Puisque les classes et les propriétés en .NET Core suivent le PascalCase,
        // nous devons s'assurer que la sérialisation utilise bien le camelCase puisque
        // c'est le format attendu par l'API REST.
        private readonly JsonSerializerOptions _jsonSerializerOptions =
            new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

        public WineService(HttpClient client, IHttpContextAccessor httpContextAccessor)
        {
            _client = client;
            _httpContextAccessor = httpContextAccessor;

            // ATTENTION:
            // L'adresse de base doit correspondre au nom du service dans le 
            // fichier "docker-compose.yml"! Le routing sera gérer par Docker.
            _client.BaseAddress = new Uri("http://vinoramix.api/");
            _client.DefaultRequestHeaders.Add("User-Agent", "Vinoramix.Web");

            // Si l'utilisateur est authentifié, joindre le jeton d'accès JWT
            // aux HTTP Headers.
            if (_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                    "Bearer",
                    _httpContextAccessor.HttpContext.User.Claims?.Single(_ => _.Type == "access_token")?.Value);
            }
        }

        public async Task<IList<Wine>> GetWinesAsync()
        {
            var response = await _client.GetAsync("/api/wines");

            response.EnsureSuccessStatusCode();

            // Désérialiser le contenu de la réponse.
            using var responseStream = await response.Content.ReadAsStreamAsync();
            return await JsonSerializer.DeserializeAsync<IList<Wine>>(responseStream, _jsonSerializerOptions);
        }

        public async Task CreateAsync(Wine wine)
        {
            // Sérialiser les valeurs afin les joindre au contenu de la requête.
            var content = new StringContent(
                JsonSerializer.Serialize(wine, _jsonSerializerOptions),
                Encoding.UTF8,
                "application/json");

            using var response = await _client.PostAsync("/api/wines", content);

            response.EnsureSuccessStatusCode();
        }

        public async Task<Wine> GetWine(int id)
        {
            var response = await _client.GetAsync($"/api/wines/{id}");

            response.EnsureSuccessStatusCode();

            // Désérialiser le contenu de la réponse.
            using var responseStream = await response.Content.ReadAsStreamAsync();
            return await JsonSerializer.DeserializeAsync<Wine>(responseStream, _jsonSerializerOptions);
        }

        public async Task UpdateAsync(Wine wine)
        {
            // Sérialiser les valeurs afin les joindre au contenu de la requête.
            var content = new StringContent(
                JsonSerializer.Serialize(wine, _jsonSerializerOptions),
                Encoding.UTF8,
                "application/json");

            using var response = await _client.PutAsync($"/api/wines/{wine.Id}", content);

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteAsync(Wine wine)
        {
            using var response = await _client.DeleteAsync($"/api/wines/{wine.Id}");

            response.EnsureSuccessStatusCode();
        }
    }
}
