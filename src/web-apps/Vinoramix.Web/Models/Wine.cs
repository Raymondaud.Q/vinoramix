﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vinoramix.Web.Models
{
    public class Wine
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int Vintage { get; set; }

        [Required]
        public string GrapeVariety { get; set; }

        [Required]
        public string VineyardName { get; set; }

        [Required]
        public decimal Price { get; set; }

        //public List<Review> Reviews { get; set; }
    }
}
