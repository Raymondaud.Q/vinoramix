﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Pages.Reviews
{
    public class IndexModel : PageModel
    {
        private readonly ReviewService _reviewService;

        public IndexModel(ReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        public IList<Review> Reviews { get;set; }

        public async Task OnGetAsync()
        {
            Reviews = await _reviewService.GetAllAsync();
        }
    }
}
