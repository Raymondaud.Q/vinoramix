﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Pages.Reviews
{
    public class DeleteModel : PageModel
    {
        private readonly ReviewService _reviewService;

        public DeleteModel(ReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [BindProperty]
        public Review Review { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Review = await _reviewService.GetSingle(id.GetValueOrDefault());

            if (Review == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            await _reviewService.DeleteAsync(Review);

            return RedirectToPage("./Index");
        }
    }
}
