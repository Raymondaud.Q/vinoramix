﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Wines
{
    public class IndexModel : PageModel
    {
        private readonly WineService _wineService;

        public IndexModel(WineService wineService)
        {
            _wineService = wineService;
        }

        public IList<Wine> Wines { get;set; }

        public async Task OnGetAsync()
        {
            Wines = await _wineService.GetWinesAsync();
        }
    }
}
