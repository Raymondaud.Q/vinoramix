﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Wines
{
    public class EditModel : PageModel
    {
        private readonly WineService _wineService;

        public EditModel(WineService wineService)
        {
            _wineService = wineService;
        }

        [BindProperty]
        public Wine Wine { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Wine = await _wineService.GetWine(id.GetValueOrDefault());

            if (Wine == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _wineService.UpdateAsync(Wine);

            return RedirectToPage("./Index");
        }
    }
}
