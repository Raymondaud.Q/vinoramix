﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Pages.Account
{
    // NOTE:
    // Cette page/classe a été générée pour nous par la fonctionnalité 
    // "Add New Scaffolded Item...", mais nous l'avons modifiée
    // afin de se connecter à notre API REST pour l'authentification
    // et l'autorisation.

    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly UserService _userService;
        private readonly ILogger<LoginModel> _logger;

        public LoginModel(UserService userService,
            ILogger<LoginModel> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            public string Username { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing cookie to ensure a clean login process.
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                // Passer les identifiants d'accès à l'API REST afin 
                // qu'il les valide.
                var result = await _userService.LoginAsync(
                    new LoginRequest()
                    {
                        Username = Input.Username,
                        Password = Input.Password
                    });

                // Est-ce que l'utilisateur existait et qu'on a reçu un jeton
                // d'accès?
                if (result != null && !string.IsNullOrEmpty(result.Token))
                {
                    // Extraire les revendications (Claims) du jeton.
                    var handler = new JwtSecurityTokenHandler();
                    var token = handler.ReadJwtToken(result.Token);

                    // Est-ce que le jeton contient un claim pour le rôle et 
                    // est-ce qu'un de ces rôles est "Administrator"?
                    if (token.Claims.Where(_ => _.Type == ClaimTypes.Role).Any(_ => _.Value == "Administrator"))
                    {
                        // Garder le nom d'utilisateur et le jeton d'accès 
                        // dans les cookies sécurisés.
                        // https://www.red-gate.com/simple-talk/dotnet/net-development/using-auth-cookies-in-asp-net-core/
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, Input.Username),
                            new Claim("access_token", result.Token)
                        };
                        var claimsIdentity = new ClaimsIdentity(
                            claims,
                            CookieAuthenticationDefaults.AuthenticationScheme);
                        var authProperties = new AuthenticationProperties();

                        // Signaler une connexion réussie au contexte HTTP géré 
                        // par ASP.NET Core. Ceci permettra d'y accéder avec
                        // "HttpContext.User" à partir de maintenant.
                        await HttpContext.SignInAsync(
                            CookieAuthenticationDefaults.AuthenticationScheme,
                            new ClaimsPrincipal(claimsIdentity),
                            authProperties);

                        _logger.LogInformation("User logged in.");
                        return LocalRedirect(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "You must be an administrator to access this site.");
                        return Page();
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return Page();
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
