﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Vinoramix.Web.Services;

namespace Vinoramix.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Activer les services de gestion des cookies d'authentification.
            services
                .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.Name = "Vinoramix.AuthCookieAspNetCore";

                    // Assurer une gestion sécuritaire des cookies.
                    // https://www.red-gate.com/simple-talk/dotnet/net-development/using-auth-cookies-in-asp-net-core/
                    options.Cookie.HttpOnly = true;
                    options.Cookie.SecurePolicy = true ? CookieSecurePolicy.None : CookieSecurePolicy.Always;
                    options.Cookie.SameSite = SameSiteMode.Lax;

                    // Redirection des utilisateurs lors de connexions et déconnexions des cookies.
                    options.LoginPath = "/Account/Login";
                    options.LogoutPath = "/Account/Logout";
                });

            // Ajout des classes d'appels au service web dans la liste de Dependency Injection.
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection
            services.AddHttpClient<WineService>();
            services.AddHttpClient<ReviewService>();
            services.AddHttpClient<UserService>();

            // Permet d'accéder au context HTTP à partir de nos modules.
            services.AddHttpContextAccessor();

            // Ajout d'un filtre d'authorisation par défault sur toutes les pages
            // de l'application web.
            // https://docs.microsoft.com/en-us/aspnet/core/razor-pages/filter#implement-razor-page-filters-globally
            services
                .AddRazorPages()
                .AddMvcOptions(options => options.Filters.Add(new AuthorizeFilter()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Puisque HTTPS nécessite l'installation et l'autorisation préalable
            // de certificats SSL sur la machine de développement, nous l'avons
            // temporairement désactivé tant que les étapes ne sont pas décrites
            // dans le README. La fonctionnalité a été testée sous Windows 10 avec
            // l'utilisation implicite des certificats SSL d'ASP.NET Core.
            // https://docs.microsoft.com/en-us/aspnet/core/security/docker-compose-https
            //TODO: Activer cette ligne une fois que HTTPS sera configuré sur Linux chez Quentin.
            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy(
                new CookiePolicyOptions
                {
                    // Ces propriétés sont essentielles afin de sécuriser les cookies.
                    // https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/quality-rules/ca5383
                    HttpOnly = HttpOnlyPolicy.Always,
                    //TODO: Activer cette ligne une fois que HTTPS sera configuré sur Linux chez Quentin.
                    //Secure = CookieSecurePolicy.Always
                });

            // Activer l'authentification et l'autorisation dans l'application web.
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
