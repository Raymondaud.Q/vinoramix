# vinoramix

Vinoramix let you know the secrets of the wines you taste !

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## Vinoramix Dev Setup

- Open the [folder](https://gitlab.com/uapv/vinoramix/-/tree/master/src/mobile/vinoramix) with Android Studio 
Android Studio should have Flutter plugins installed and Flutter Engine linked according to the [online documentation](https://flutter.dev/docs)
Enable Dev mode on Android, USB debugging mode and Usb File Transfer Mode.
You are now able to build APK and edit the src code in /lib

By default APK are builts in /vinoramix/build/app/outputs/flutter-apk/app-release.apk 


## APK Link

- Download [APK](https://drive.google.com/file/d/1u58icTLRtAqRM6zPqj38sTLvAWB7DAY7/view?usp=sharing)
Install on your phone and setup the [Back-End](https://gitlab.com/uapv/vinoramix/-/tree/master/)  
Now, you can test our entire work.

## Ingineering Document Link

- See our [document here](https://gitlab.com/uapv/vinoramix/-/blob/master/Urbanisation_TURCOTTE_RAYMONDAUD.pdf)

## Screenshots

- [Some ScreenShots are available here](https://gitlab.com/uapv/vinoramix/-/tree/master/src/mobile/Screenshots)
