// Quentin RATMONDAUD
// Minimal ListView<Wine> example

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vinoramix/httpservice/wine_object.dart';
import 'package:vinoramix/view/wine_details.dart';

class WineList extends StatefulWidget {
  final List<Wine> wine;

  WineList( {Key key, this.wine}) : super(key: key);

  @override
  _WineListState createState() => _WineListState();
}

class _WineListState extends State<WineList> {
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return new ListView.builder(
        itemCount: widget.wine == null ? 0 : widget.wine.length,
        itemBuilder: (BuildContext context, int index) {
          return new Container(
              padding: EdgeInsets.all(5),
              child: new RaisedButton(
                onPressed: () => showDetail(widget.wine[index]),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(1.0),
                  side: BorderSide(width: 0.2, color: Colors.black),
                ),
                color: Colors.amber[50],
                child: new Container(
                  height: 150,
                  padding: EdgeInsets.all(7),
                  child: new Center(
                      child: new Column(
                        // Stretch the cards in horizontal axis
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(width: 0.1, color: Colors.black),
                            ),
                            color: Colors.amberAccent,
                            child: new Padding(
                              padding: EdgeInsets.all(5),
                              child: new Text(
                                // Read the name field value and set it in the Text widget
                                widget.wine[index].name,
                                textAlign: TextAlign.center,

                                // set some style to text
                                style: new TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Container(
                              decoration: deco(),
                              padding: EdgeInsets.all(5),
                              margin: EdgeInsets.all(5),
                              child: ButtonBar(alignment: MainAxisAlignment.center, children: [
                                Text("Cuvée: " + widget.wine[index].vintage.toString(), style:style()),
                                Text("Vineyard: " + widget.wine[index].vineyardName, style:style()),
                                Text("Price: " + widget.wine[index].price.toString() + "€", style:style())
                              ])),
                        ],
                      )),
                ),
              ));
        });
  }

  void showDetail(Wine w) {
    Navigator.push(
      this.context,
      MaterialPageRoute(builder: (context) => WinePage(w)),
    );
  }

  BoxDecoration deco() {
    return BoxDecoration(
      border: Border.all(
        color: Colors.amber, //                   <--- border color
        width: 5.0,
      ),
    );
  }

  TextStyle style(){
    return TextStyle(
        fontSize: 10.0,
        color: Colors.black54,
        fontStyle: FontStyle.italic,
        fontWeight: FontWeight.w400);
  }
}
