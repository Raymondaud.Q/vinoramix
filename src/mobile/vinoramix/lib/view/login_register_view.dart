// RAYMONDAUD Quentin
// Login Page
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:localstorage/localstorage.dart';
import 'package:vinoramix/httpservice/http_service.dart';
import 'package:vinoramix/httpservice/session_object.dart';
import 'package:vinoramix/view/ocr_view.dart';


// Widget Login Page
class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

// Used for controlling whether the user is loggin or creating an account
enum FormType { login, register }


// Contenu du widget
class _LoginPageState extends State<LoginPage> {
  final LocalStorage storage = new LocalStorage('todo_app');
  // Controller de champs de texte
  final TextEditingController _emailFilter = new TextEditingController();
  final TextEditingController _usernameFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();
  final TextEditingController _ipPortFilter = new TextEditingController();
  // Données des champs de texte
  String _email = "";
  String _username = "";
  String _password = "";
  String _ipPort = "";
  String _invalid = "";
  // Login form par défaut
  FormType _form = FormType
      .login; // our default setting is to login, and we should switch to creating an account when the user chooses to

  // Initialise les controller avec les listener de textfields
  _LoginPageState() {
    _usernameFilter.addListener(_usernameListen);
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
    _ipPortFilter.addListener(_testIPPORTListen);
  }

  // Textfield listener
  _usernameListen() {
    if (_usernameFilter.text.isEmpty) {
      _username = "";
    } else {
      _username = _usernameFilter.text;
    }
  }

  // Textfield listener
  _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  // Textfield listener
  _testIPPORTListen() {
    if (_ipPortFilter.text.isEmpty) {
      _ipPort = "";
    } else {
      _ipPort = _ipPortFilter.text;
    }
  }

  // Textfield listener
  _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  // Swap in between our two forms, registering and logging in
  _formChange() async {
    setState(() {
      if (_form == FormType.register) {
        _form = FormType.login;
        _invalid = "";
      } else {
        _form = FormType.register;
        _invalid = "";
      }
    });
  }

  // Widget content build
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: _buildBar(context),
      body: new Container(
        color: Colors.grey[300],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            _buildTextFields(),
            _buildButtons(),
          ],
        )),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      title: new Text("Vinoramix"),
      centerTitle: true,
    );
  }

  // TextField building according to state
  Widget _buildTextFields() {
    if (_form == FormType.login) {
      return new Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(30),
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.all(10),
              child: Text(
                _invalid,
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 15.0,
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new TextField(
                controller: _usernameFilter,
                decoration: new InputDecoration(
                  labelText: 'Username',
                  prefixIcon: Icon(Icons.account_circle),
                  border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                    const Radius.circular(40.0),
                  )),
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new TextField(
                  controller: _passwordFilter,
                  obscureText: true,
                  decoration: new InputDecoration(
                    labelText: 'Password',
                    prefixIcon: Icon(Icons.work_sharp),
                    border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    )),
                  )),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new TextField(
                controller: _ipPortFilter,
                decoration: new InputDecoration(
                  labelText: 'ip:port',
                  prefixIcon: Icon(Icons.electrical_services),
                  border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                    const Radius.circular(40.0),
                  )),
                ),
              ),
            )
          ],
        ),
      );
    } else if (_form == FormType.register) {
      return new Container(
        padding: EdgeInsets.all(30),
        margin: EdgeInsets.all(10),
        child: new Column(
          children: <Widget>[
            new Container(
              child: Text(
                _invalid,
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 15.0,
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new TextField(
                controller: _usernameFilter,
                decoration: new InputDecoration(
                  labelText: 'Username',
                  prefixIcon: Icon(Icons.account_circle),
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    ),
                  ),
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: Text(
                " Passwords must contains uppercase character, lowercase character, a digit, and a non-alphanumeric character be and at least six characters long.",
                textAlign: TextAlign.center,

                // set some style to text
                style: new TextStyle(
                  fontSize: 15.0,
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new TextField(
                controller: _passwordFilter,
                decoration: new InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.lock),
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    ),
                  ),
                ),
                obscureText: true,
              ),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new TextField(
                controller: _emailFilter,
                decoration: new InputDecoration(
                  labelText: 'Email',
                  prefixIcon: Icon(Icons.mail_outline),
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    ),
                  ),
                ),
              ),
            ),
            new Container(
                padding: EdgeInsets.all(10),
                child: new TextField(
                  controller: _ipPortFilter,
                  decoration: new InputDecoration(
                    labelText: 'ip:port',
                    prefixIcon: Icon(Icons.electrical_services),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(40.0),
                      ),
                    ),
                  ),
                ))
          ],
        ),
      );
    }
  }

  // Button building according to form state
  Widget _buildButtons() {
    if (_form == FormType.login) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
                padding: EdgeInsets.all(10),
                child: ButtonTheme(
                    minWidth: 400,
                    height: 50,
                    child: RaisedButton(
                        child: new Text('Login'),
                        onPressed: _loginPressed,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40),
                            side: BorderSide(color: Colors.amberAccent))))),
            ButtonTheme(
                minWidth: 400,
                height: 50,
                child: FlatButton(
                    child:
                        new Text('Dont have an account? Tap here to register.'),
                    onPressed: _formChange,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40),
                        side: BorderSide(color: Colors.amberAccent)))),
            new FlatButton(
              child: new Text('Forgot Password?'),
              onPressed: _passwordReset,
            ),
          ],
        ),
      );
    } else {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
                padding: EdgeInsets.all(10),
                child: ButtonTheme(
                    minWidth: 400,
                    height: 50,
                    child: RaisedButton(
                      child: new Text('Create an Account'),
                      onPressed: _createAccountPressed,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                          side: BorderSide(color: Colors.amberAccent)),
                    ))),
            ButtonTheme(
                minWidth: 400,
                height: 50,
                child: FlatButton(
                  child: new Text('Have an account? Click here to login.'),
                  onPressed: _formChange,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                      side: BorderSide(color: Colors.amberAccent)),
                )),
          ],
        ),
      );
    }
  }

  // Amène sur la vue définie dans ocr_view.dart
  _cameraVision() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => OCR()),
    );
  }

  // Call Http services et returns a Session that contains a token, expiration date & more
  _loginPressed() async {
    if (_username != "" && _password != "" && _ipPort != "") {
      print(
          'The user wants to login with $_username and $_password on http://$_ipPort/api/auth/login');
      Session user = await HttpService.login(_username, _password, _ipPort);
      if (user != null) {
        storage.setItem('session', {
          'username': user.username,
          'token': user.token,
          'expiration': user.expiration
        });
        storage.setItem('ipport', _ipPort); // For testing
        invalidNeutral();
        _cameraVision();
      } else
        invalidLogin();
    }
  }

  // Call http service for creating an account & watch for reponse status
  _createAccountPressed() async {
    if (_username != "" && _password != "" && _ipPort != "" && _email != "") {
      print(
          'The user wants to create an account with $_email, $_username and $_password on http://$_ipPort/api/auth/register');
      Map<String, dynamic> resp =
          await HttpService.register(_username, _password, _email, _ipPort);
      if (resp == null || resp['status'] == "Error") {
        print(_invalid);
        invalidRegister();
      } else {
        invalidNeutral();
        _formChange();
      }
    }
  }

  // UI WARNING Messages
  invalidRegister() {
    setState(() {
      _invalid =
          "Erreur ! Respectez les règles de sécurité ou retrouvez votre mot de passe";
    });
  }

  invalidLogin() {
    setState(() {
      _invalid = "Erreur ! Mot de passe et/ou login incorrects";
    });
  }

  invalidNeutral() {
    setState(() {
      _invalid = "";
    });
  }

  void _passwordReset() {
    print("The user wants a password reset request sent to $_email");
  }
}
