// RAYMONDAUD Quentin
// Vinoramix minimal session object

// Minimal Session Object data structure
// We can add picture, status or a custom Author(/review_object.dart) object according to our informations
class Session{
  String token;
  String username;
  String expiration;
  Session({this.token, this.expiration, this.username});

  // Parse Map<> and returns Session objet
  factory Session.fromJson(Map<String, dynamic> json, String uName){
    if ( json["token"] != null )
      return Session(token: json["token"],
                     expiration: json["expiration"],
                     username: uName);
    else
      return null;
  }

}

