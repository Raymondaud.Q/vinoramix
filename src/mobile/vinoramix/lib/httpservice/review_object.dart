// RAYMONDAUD Quentin
// Vinoramix Review object


// Minimal Review Data Object Structure
class Review {
  int id;
  Author author;
  int note;
  String comment;
  String createdOn;

  // Constructor
  Review({this.id, this.author, this.note, this.comment, this.createdOn});

  // Parse Map<> and returns a Review
  factory Review.fromJson(Map<String, dynamic> json) {
    if (json != null)
      return Review(
          id: json["id"],
          author: Author.fromJson(json["author"]),
          note: json["note"],
          comment: json["comment"],
          createdOn: json["createdOn"]);
    return null;
  }
  static List<Review> ReviewsFromJson(List<dynamic> str) =>
      List<Review>.from(str.map((x) => Review.fromJson(x)));

  static List<Review> toJson(List<Review> reviews) {

  }
}

// Minmal Author Data Obkect Structure
// Could contains "Profile" informations such as Profile img, Status, Biography, ...
class Author {
  String id;
  String username;

  //Constructor
  Author({this.id, this.username});

  //Parse Map<> and returns an Author
  factory Author.fromJson(Map<String, dynamic> json) {
    if (json != null) return Author(id: json["id"], username: json["username"]);
    return null;
  }
}
