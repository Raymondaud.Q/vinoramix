﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Authentication;
using Vinoramix.Api.Models.Users;

namespace Vinoramix.Api.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;

        public AuthenticationController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            var user = await _userManager.FindByNameAsync(loginRequest.Username);

            if (user != null && await _userManager.CheckPasswordAsync(user, loginRequest.Password))
            {
                var roles = await _userManager.GetRolesAsync(user);
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                foreach(var role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }

                var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Authentication:SigningKey"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["Authentication:ValidIssuer"],
                    audience: _configuration["Authentication:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: claims,
                    signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                );

                return Ok(new {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }

            return Unauthorized();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterRequest registerRequest)
        {
            User user = await _userManager.FindByNameAsync(registerRequest.Username);

            if (user != null)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError, 
                    new RegisterResponse()
                    {
                        Status = "Error",
                        Message = "User already exists!"
                    }
                );
            }

            user = new User()
            {
                UserName = registerRequest.Username,
                Email = registerRequest.Email,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = await _userManager.CreateAsync(user, registerRequest.Password);
            if (!result.Succeeded)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new RegisterResponse()
                    {
                        Status = "Error",
                        Message = "User creation failed! Please check user details and try again."
                    }
                );
            }

            if (!(await AssignRoleAsync(user, UserRoles.User)).Succeeded)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new RegisterResponse()
                    {
                        Status = "Error",
                        Message = "User creation failed! Could not assign specific roles."
                    }
                );
            }

            return Ok(new RegisterResponse()
            {
                Status = "Success",
                Message = "User created successfully!"
            });
        }

        [Authorize(Roles = UserRoles.Administrator)]
        [HttpPost]
        [Route("register/admin")]
        public async Task<IActionResult> RegisterAdministrator([FromBody] RegisterRequest registerRequest)
        {
            User user = await _userManager.FindByNameAsync(registerRequest.Username);

            if (user != null)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new RegisterResponse()
                    {
                        Status = "Error",
                        Message = "User already exists!"
                    }
                );
            }

            user = new User()
            {
                UserName = registerRequest.Username,
                Email = registerRequest.Email,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = await _userManager.CreateAsync(user, registerRequest.Password);
            if (!result.Succeeded)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new RegisterResponse()
                    {
                        Status = "Error",
                        Message = "User creation failed! Please check user details and try again."
                    }
                );
            }

            if (!(await AssignRoleAsync(user, UserRoles.User)).Succeeded ||
                !(await AssignRoleAsync(user, UserRoles.Administrator)).Succeeded)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new RegisterResponse()
                    {
                        Status = "Error",
                        Message = "User creation failed! Could not assign specific roles."
                    }
                );
            }

            return Ok(new RegisterResponse()
            {
                Status = "Success",
                Message = "User created successfully!"
            });
        }

        private async Task<IdentityResult> AssignRoleAsync(User user, string role)
        {
            if (await _roleManager.RoleExistsAsync(role))
            {
                return await _userManager.AddToRoleAsync(user, role);
            }
            else
            {
                return IdentityResult.Failed();
            }
        }
    }
}
