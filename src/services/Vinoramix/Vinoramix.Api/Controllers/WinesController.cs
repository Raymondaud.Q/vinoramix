﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using Vinoramix.Api.Data;
using Vinoramix.Api.Data.Wines;
using Vinoramix.Api.Models.Users;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WinesController : ControllerBase
    {
        private readonly IWineRepository _wineRepository;

        public WinesController(IWineRepository wineRepository)
        {
            _wineRepository = wineRepository;
        }

        // GET: api/Wines/1/reviews
        [Authorize(Roles = UserRoles.User)]
        [HttpGet("{id}/reviews")]
        public async Task<ActionResult<IEnumerable<ReviewGetDto>>> GetReviews(int id)
        {
            var reviews = await _wineRepository.GetWineReviewsAsync(id);

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return reviews
                .Select(review => new ReviewGetDto()
                {
                    Id = review.Id,
                    Author = new UserDto()
                    {
                        Id = review.Author.Id,
                        Username = review.Author.UserName
                    },
                    CreatedOn = review.CreatedOn,
                    Note = review.Note,
                    Comment = review.Comment
                }).ToList();
        }

        // GET: api/Wines
        [Authorize(Roles = UserRoles.User)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WineGetDto>>> GetWines(string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                return await SearchAsync(query);
            }
            else
            {
                var results = await _wineRepository.GetAllAsync();

                //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
                return results
                    .Select(wine => new WineGetDto()
                    {
                        Id = wine.Id,
                        Description = wine.Description,
                        Name = wine.Name,
                        GrapeVariety = wine.GrapeVariety,
                        VineyardName = wine.VineyardName,
                        Vintage = wine.Vintage,
                        Price = wine.Price,
                        Reviews = wine.Reviews.OrderByDescending(_ => _.CreatedOn).Select(review => new ReviewGetDto()
                        {
                            Id = review.Id,
                            Author = new UserDto()
                            {
                                Id = review.Author.Id,
                                Username = review.Author.UserName
                            },
                            Note = review.Note,
                            Comment = review.Comment,
                            CreatedOn = review.CreatedOn
                        }).ToList()
                    }).ToList();
            }
        }

        private async Task<ActionResult<IEnumerable<WineGetDto>>> SearchAsync(string query)
        {
            var results = await _wineRepository.GetSearchResultsAsync(query);

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return results
                .Select(wine => new WineGetDto()
                {
                    Id = wine.Id,
                    Description = wine.Description,
                    Name = wine.Name,
                    GrapeVariety = wine.GrapeVariety,
                    VineyardName = wine.VineyardName,
                    Vintage = wine.Vintage,
                    Price = wine.Price,
                    Reviews = wine.Reviews.OrderByDescending(_ => _.CreatedOn).Select(review => new ReviewGetDto()
                    {
                        Id = review.Id,
                        Author = new UserDto()
                        {
                            Id = review.Author.Id,
                            Username = review.Author.UserName
                        },
                        Note = review.Note,
                        Comment = review.Comment,
                        CreatedOn = review.CreatedOn
                    }).ToList()
                }).ToList();
        }

        [Authorize(Roles = UserRoles.User)]
        [HttpGet("top")]
        public async Task<ActionResult<WineGetDto>> SearchTopOnlyAsync(string query)
        {
            var results = (await SearchAsync(query)).Value;

            // Rien à faire si l'on ne reçoit pas de résultats.
            if (results.Count() == 0)
            {
                return null;
            }

            // Nettoyer la requête.
            var terms = query.Trim().ToLower().Split(";");

            // Fonction hyper-basique pour déterminer le score des résultats
            // afin de retourner LE résultats idéal au client.
            //TODO: Implémenter une meilleur stratégie comme Lucene.Net.
            WineGetDto topResult = null;
            int maxTermMatches = 0;
            foreach (var item in results)
            {
                // Compter le nombre de termes qui match ce vin.
                var count = terms.Count(term =>
                    item.Name.ToLower().Contains(term) ||
                    item.GrapeVariety.ToLower().Contains(term) ||
                    item.VineyardName.ToLower().Contains(term) ||
                    item.Vintage.ToString().Contains(term));

                if (count > maxTermMatches)
                {
                    maxTermMatches = count;
                    topResult = item;
                }
            }

            return topResult;
        }

        // GET: api/Wines/5
        [Authorize(Roles = UserRoles.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<WineGetDto>> GetWine(int id)
        {
            var wine = await _wineRepository.GetByIdAsync(id);

            if (wine == null)
            {
                return NotFound();
            }

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return new WineGetDto()
            {
                Id = wine.Id,
                Description = wine.Description,
                Name = wine.Name,
                GrapeVariety = wine.GrapeVariety,
                VineyardName = wine.VineyardName,
                Vintage = wine.Vintage,
                Price = wine.Price,
                Reviews = wine.Reviews?.Select(review => new ReviewGetDto()
                {
                    Id = review.Id,
                    Author = new UserDto()
                    {
                        Id = review.Author.Id,
                        Username = review.Author.UserName
                    },
                    Note = review.Note,
                    Comment = review.Comment,
                    CreatedOn = review.CreatedOn
                })?.ToList()
            };
        }

        // PUT: api/Wines/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = UserRoles.Administrator)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWine(int id, WineUpdateDto wine)
        {
            if (id != wine.Id)
            {
                return BadRequest();
            }

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            var editWine = new Wine()
            {
                Id = wine.Id,
                Name = wine.Name,
                Description = wine.Description,
                VineyardName = wine.VineyardName,
                GrapeVariety = wine.GrapeVariety,
                Vintage = wine.Vintage,
                Price = wine.Price
            };

            if (!await _wineRepository.UpdateAsync(editWine))
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Wines
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = UserRoles.Administrator)]
        [HttpPost]
        public async Task<ActionResult<WineCreateDto>> PostWine(WineCreateDto wine)
        {
            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            var newWine = new Wine()
            {
                Name = wine.Name,
                Description = wine.Description,
                VineyardName = wine.VineyardName,
                GrapeVariety = wine.GrapeVariety,
                Vintage = wine.Vintage,
                Price = wine.Price
            };

            await _wineRepository.CreateAsync(newWine);

            return CreatedAtAction(
                "GetWine", 
                new { id = newWine.Id }, 
                new WineCreateDto()
                {
                    Name = wine.Name,
                    Description = wine.Description,
                    VineyardName = wine.VineyardName,
                    GrapeVariety = wine.GrapeVariety,
                    Vintage = wine.Vintage,
                    Price = wine.Price
                });
        }

        // DELETE: api/Wines/5
        [Authorize(Roles = UserRoles.Administrator)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<WineCreateDto>> DeleteWine(int id)
        {
            var deletedWine = await _wineRepository.DeleteAsync(id);

            if (deletedWine == null)
            {
                return NotFound();
            }

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return new WineCreateDto()
            {
                Name = deletedWine.Name,
                Description = deletedWine.Description,
                VineyardName = deletedWine.VineyardName,
                GrapeVariety = deletedWine.GrapeVariety,
                Vintage = deletedWine.Vintage,
                Price = deletedWine.Price
            };
        }
    }
}
