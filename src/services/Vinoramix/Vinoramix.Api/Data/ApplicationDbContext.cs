﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Vinoramix.Api.Models.Users;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Wine> Wines { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
            => base.OnModelCreating(modelBuilder);
    }
}
