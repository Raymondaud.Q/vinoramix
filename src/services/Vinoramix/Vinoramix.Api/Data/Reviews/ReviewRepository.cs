﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data.Wines
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly ApplicationDbContext _context;

        public ReviewRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Review review)
        {
            _context.Reviews.Add(review);
            await _context.SaveChangesAsync();
        }

        public async Task<IList<Review>> GetAllAsync()
        {
            return await _context.Reviews
                .AsNoTracking()
                .Include(_ => _.Author)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<Review> GetByIdAsync(int id)
        {
            return await _context.Reviews
                .AsNoTracking()
                .Include(_ => _.Author)
                .SingleOrDefaultAsync(_ => _.Id == id)
                .ConfigureAwait(false);
        }

        public async Task<Review> DeleteAsync(int id)
        {
            var review = await _context.Reviews.FindAsync(id);

            if (review == null)
            {
                return null;
            }

            _context.Reviews.Remove(review);
            await _context.SaveChangesAsync();

            return review;
        }

        public Task<bool> ExistsAsync(int id)
        {
            return _context.Wines.AnyAsync(e => e.Id == id);
        }

        public async Task<bool> UpdateAsync(Review review)
        {
            _context.Entry(review).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await ExistsAsync(review.Id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }
    }
}
