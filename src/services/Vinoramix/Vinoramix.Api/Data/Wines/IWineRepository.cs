﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data.Wines
{
    public interface IWineRepository
    {
        Task CreateAsync(Wine wine);

        Task<Wine> GetByIdAsync(int id);

        Task<IList<Wine>> GetAllAsync();

        Task<bool> UpdateAsync(Wine wine);

        Task<Wine> DeleteAsync(int id);

        Task<IList<Wine>> GetSearchResultsAsync(string query);

        Task<IEnumerable<Review>> GetWineReviewsAsync(int id);

        Task<bool> ExistsAsync(int id);
    }
}
