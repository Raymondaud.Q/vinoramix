﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data.Wines
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string GetUserIdByUsername(string username)
        {
            return _context.Users
                .Where(_ => _.UserName == username)
                .Select(_ => _.Id)
                .Single();
        }
    }
}
