using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Vinoramix.Api.Data;
using Vinoramix.Api.Data.Wines;
using Vinoramix.Api.Models.Users;

namespace Vinoramix.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();

            // Add PostgresSQL support.
            var connectionString = Configuration["DATABASE_URL"];
            services
                .AddEntityFrameworkNpgsql()
                .AddDbContext<ApplicationDbContext>(
                    options => options.UseNpgsql(connectionString)
                )
                .AddTransient<IWineRepository, WineRepository>()
                .AddTransient<IReviewRepository, ReviewRepository>()
                .AddTransient<IUserRepository, UserRepository>();

            // Add Identity support.
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add authentication to the ASP.NET Core application.
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options => {
                // Add JWT Bearer Tokens to the ASP.NET Core application.
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Authentication:ValidIssuer"],
                    ValidateAudience = true,
                    ValidAudience = Configuration["Authentication:ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["Authentication:SigningKey"])
                    )
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Puisque HTTPS n�cessite l'installation et l'autorisation pr�alable
            // de certificats SSL sur la machine de d�veloppement, nous l'avons
            // temporairement d�sactiv� tant que les �tapes ne sont pas d�crites
            // dans le README. La fonctionnalit� a �t� test�e sous Windows 10 avec
            // l'utilisation implicite des certificats SSL d'ASP.NET Core.
            // https://docs.microsoft.com/en-us/aspnet/core/security/docker-compose-https
            //TODO: Activer cette ligne une fois que HTTPS sera configur� sur Linux chez Quentin.
            //app.UseHttpsRedirection();

            app.UseRouting();

            // Activer l'authentification et l'autorisation dans l'application web.
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors(
                builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                }
            );

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
