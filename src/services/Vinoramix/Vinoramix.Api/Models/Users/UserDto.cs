﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vinoramix.Api.Models.Users
{
    public class UserDto
    {
        public string Id { get; set; }

        public string Username { get; set; }
    }
}
