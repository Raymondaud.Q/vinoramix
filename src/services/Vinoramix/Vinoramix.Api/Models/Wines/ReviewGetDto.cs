﻿using System;
using System.ComponentModel.DataAnnotations;
using Vinoramix.Api.Models.Users;

namespace Vinoramix.Api.Models.Wines
{
    public class ReviewGetDto
    {
        public int Id { get; set; }

        public UserDto Author { get; set; }

        [Range(0.0f, 5.0f, ErrorMessage = "Note must be between 0 and 5")]
        public float Note { get; set; }

        [MaxLength(4000, ErrorMessage = "Comment cannot exceed 4000 characters")]
        public string Comment { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
