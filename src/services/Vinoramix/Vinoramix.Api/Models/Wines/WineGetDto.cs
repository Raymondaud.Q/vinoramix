﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vinoramix.Api.Models.Wines
{
    public class WineGetDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Vintage { get; set; }

        public string GrapeVariety { get; set; }

        public string VineyardName { get; set; }

        public decimal Price { get; set; }

        public List<ReviewGetDto> Reviews { get; set; }
    }
}
