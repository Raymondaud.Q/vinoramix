## Introduction

Vinoramix est une application pour les amateurs de vins. Elle permet d'accéder aux détails d'une bouteille 
de vin en scannant son étiquette avec un smartphone.

## Exécution des conteneurs Docker

1. Installer Docker et Docker-Compose.
2. Créer un fichier `.env` pour les variables d'environnements. *(Voir section suivante bas.)*  
   **OU Pour vous faciliter la tâche ici vous pouvez juste lancer la commande `mv env .env`si vous voulez utiliser notre exemple**
3. Lancer les conteneurs avec Docker-Compose. `docker-compose up -d`  
    L'api Web est accessible sur le `port 9000`  
    Adminer est disponible sur `localhost:8080` 
4. Télécharger et installer l'[APK](https://drive.google.com/file/d/1u58icTLRtAqRM6zPqj38sTLvAWB7DAY7/view?usp=sharing)
5. Identifiez vous avec `ID: "user1", Pass: "Password1$", IP:PORT: "<Your Server IP>:9000"` ou créez vous un compte.

- Assurez vous que vous êtes sur le même réseau que le serveur,  
ou du moins que votre téléphone puisse joindre le serveur via Internet

### Fichiers de variables d'environnements pour Docker Compose `.env`

Pour éviter une faille de sécurité en joignant des secrets dans le code source sur Git, nous devons extraire
les valeurs sensibles du fichier `.env`. Ce fichier doit obligatoirement être placé à la racine de votre
répertoire, côte à côte avec le fichier `docker-compose.yml` pour le lancement de la commande `docker-compose up -d`.  

Pour des raisons de praticité, fournissons un fichier nommé `env` à la racine contenant les données sensibles.
Ces données ne devraient pas être sur le dépot si le serveur fournissait réellement les services.

#### Fichier d'exemple `.env.sample`

Vous pouvez vous baser sur le fichier `.env.sample` en le copiant et renommant cette copie `.env`. Vous devrez
ensuite modifier les valeurs de variables à l'intérieur.

```
POSTGRES_USER=**YOUR_USER**
POSTGRES_PASSWORD=**YOUR_PASSWORD**
DATABASE_URL=Host=postgresql.data;Database=vinoramix;Username=**YOUR_USER**;Password=**YOUR_PASSWORD**
Authentication__ValidAudience=http://vinoramix.web
Authentication__ValidIssuer=http://vinoramix.web
Authentication__SigningKey=**YOUR_JWT_TOKEN_SIGNING_KEY_SHOULD_BE_SECURE_AND_STRONG_ENOUGH**
```

Veillez à remplacer les valeurs `**YOUR_USER**`, `**YOUR_PASSWORD**` et `**YOUR_JWT_TOKEN_SIGNING_KEY_SHOULD_BE_SECURE_AND_STRONG_ENOUGH**`.


### Mise en route rapide



## Structure du dépôt Git

### Conteneurs Docker

Le fichier `docker-compose.yml` permettant le développement avec Visual Studio Community et les tests locaux 
se trouve à la racine du dépot.

Il existe aussi une version de déploiement mettant en place `nginx` et HTTPS, mais il requiert l'installation de 
certificats SSL sur la machine et de les authoriser. Cette version se trouve sous `/deploy` avec les fichier de
configuration de `nginx`.

Les fichiers Dockerfile se trouvent à la racine des projets respectifs pour l'application web et l'API REST, soit 
`/src/web-apps/Vinoramix.Web/Dockerfile` et `/src/services/Vinoramix/Vinoramix.Api/Dockerfile`.


### API REST

- [Le code pour l'API REST](https://gitlab.com/uapv/vinoramix/-/tree/master/src/services/Vinoramix/Vinoramix.Api) se trouve sous le répertoire `/src/services/Vinoramix/Vinoramix.Api`.

### Application Web

- [Le code pour l'application web administrative](https://gitlab.com/uapv/vinoramix/-/tree/master/src/web-apps/Vinoramix.Web) se trouve sous le répertoire `/src/web-apps/Vinoramix.Web`.

### Application Mobile


- [Le code et les informations concernant l'application mobile](https://gitlab.com/uapv/vinoramix/-/tree/master/src/mobile/vinoramix) se trouve sous le répertoire `/src/mobile/vinoramix` et plus précisément 
sous le répertoire `/src/mobile/vinoramix/lib`.

- Vous pouvez jeter un oeil a nos [Captures d'écran](https://gitlab.com/uapv/vinoramix/-/tree/master/src/mobile/Screenshots)
